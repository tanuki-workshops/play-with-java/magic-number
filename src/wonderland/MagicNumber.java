package wonderland;

import java.lang.String;
import java.lang.System;

public class MagicNumber {
    public String fullySpelledOut() {
        return "fourty two ✨💥🦄";
    }

    public void printMe() {
        System.out.println(this.fullySpelledOut());
    }

    public static void main( String[] args )
    {
        MagicNumber myNumber = new MagicNumber();
        System.out.println(myNumber.fullySpelledOut());
    }

}
